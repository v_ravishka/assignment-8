#include <stdio.h>
struct student {
    int stdNum;
    char firstName[50];
    char subject[20];
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter five student details:\n");


    for (i = 0; i < 5; ++i) {
        s[i].stdNum = i + 1;
        printf("\nstudent no %d \n", s[i].stdNum);
        printf("Enter first name: ");
        scanf("%s", s[i].firstName);
        printf("Enter Subject: ");
        scanf("%s", s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("\n============================================================\n");
    printf("\nStudent Information:\n");
    printf("\n============================================================\n");

    for (i = 0; i < 5; ++i) {
        printf("\nStudent number: %d\n", i + 1);
        printf("First name: ");
        puts(s[i].firstName);
        printf("Subject : ");
        puts(s[i].subject);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
